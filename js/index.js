"use strict"

//1. Цикли - це простий спосіб зробити якусь дію кілька разів.
//2. Цикли є for, do while, while.
//3. Цикл while дозволя ініціалізувати змінні лічильника перед початком тіла циклу.
// Цикл do while дозволяє ініціалізувати змінні лічильника до та після запуску тіла циклу.

let num1, num2;

do {
    num1 = parseFloat(prompt("Введіть перше число:"));
} while (isNaN(num1));

do {
    num2 = parseFloat(prompt("Введіть друге число:"));
} while (isNaN(num2));

let minNum = Math.min(num1, num2);
let maxNum = Math.max(num1, num2);

console.log("Цілі числа від " + minNum + " до " + maxNum + ":");

for (let i = Math.floor(minNum); i <= maxNum; i++) {
    console.log(i);
}





let userNumber;
let isEven = false;

do {
    userNumber = prompt("Введіть число:");

    if (!isNaN(userNumber)) {
        isEven = userNumber % 2 === 0;
        if (!isEven) {
            alert("Введене число не є парним. Будь ласка, введіть парне число.");
        }
    } else {
        alert("Введене значення не є числом. Будь ласка, введіть коректне число.");
    }

} while (!isEven);

console.log("Ви ввели парне число: " + userNumber);
